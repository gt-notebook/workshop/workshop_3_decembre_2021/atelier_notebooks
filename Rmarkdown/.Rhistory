data1 <- query_wikidata('
SELECT ?identifiant ?image  WHERE {
SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
?identifiant wdt:P31 wd:Q5 ; wdt:P373 "Johannes Vermeer" .
?identifiant wdt:P18 ?image .
}
')
# Chunk 11: displaydata
# Affichage dynamique de la table
library(DT)
datatable(data1)
# Chunk 12: datafolder
# Création des sous-répertoires 'data' et 'data/image'
dir.create("data")
dir.create("data/images")
# Chunk 13
# Téléchargement et sauvegarde de l'image sur sa machine
download.file(url = data1$image,
destfile = 'data/images/portrait_artiste.jpg',
mode = 'wb')
# Chunk 14
include_graphics('data/images/portrait_artiste.jpg')
# Chunk 15: sparql2
# Requête SPARQL
# Recherche de toutes les œuvres de Johannes Vermeer et de leurs localisations (musée)
data2 <- query_wikidata('
SELECT ?oeuvreLabel ?museeLabel ?coord
WHERE { SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
?oeuvre wdt:P170 wd:Q41264 .
OPTIONAL {?oeuvre p:P276 ?loc . ?loc ps:P276 ?musee . ?musee wdt:P625 ?coord} .
}
')
# Chunk 16: displaydata3
# Affichage de la table
datatable(data2)
# Chunk 17: sparql3
# Requête SPARQL
# Recherche de toutes les localisations + dates associées à
# l'œuvre "A Young Woman Seated at the Virginals" de Johannes Vermeer
data <- query_wikidata('
SELECT ?image ?nomLabel ?dated ?datef ?coord WHERE {
SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
wd:Q4660880 p:P276 ?location ; wdt:P18 ?image .
?location ps:P276 ?nom .
?location pq:P580 ?dated .
?location pq:P582 ?datef .
?nom wdt:P625 ?coord .
}
')
# Chunk 18: displaydata1
# Affichage de la table
datatable(data)
# Chunk 19: writedata
# Enregistrement des données en local
write.csv(x = data, file = "data/data.csv", row.names = FALSE)
# Import du fichier de données enregistré
data <- read.csv("data/data.csv", row.names = NULL)
# Chunk 20
# Téléchargement et sauvegarde en local de l'image
download.file(url = data$image[1],
destfile = 'data/images/Q4660880.jpg',
mode = 'wb')
# Chunk 21
include_graphics("data/images/Q4660880.jpg")
# Chunk 22
# Détection/Suppression doublon(s)
data2 <- data2[!duplicated(data2$oeuvreLabel), ]
# Chunk 23
# Suppression NA
data2 <- data2[!is.na(data2$coord), ]
# Chunk 24
datatable(data2)
# Chunk 25
# Regroupement - Calcul du nombre œuvres par musée
nb_oeuvre_by_musee <- aggregate(oeuvreLabel ~ coord + museeLabel, data = data2, FUN = length)
# Affichage du résultat
datatable(nb_oeuvre_by_musee)
# Chunk 26
# Suppression des chaînes de caractères "Point(" et ")"
nb_oeuvre_by_musee$coord <- gsub(pattern = 'Point\\(', replacement = '', nb_oeuvre_by_musee$coord)
nb_oeuvre_by_musee$coord <- gsub(pattern = '\\)', replacement = '', nb_oeuvre_by_musee$coord)
# Séparation longitude et latitude en deux colonnes
out <- strsplit(nb_oeuvre_by_musee$coord, split = ' ')
musee_geo <- cbind(nb_oeuvre_by_musee, do.call(rbind, out))
# Renommage de colonnes créées
colnames(musee_geo)[4:5] <- c('long', 'lat')
# Chunk 27
datatable(musee_geo)
# Chunk 28
# Package pour la gestion de données géographique
library(sf)
# Géoréférencement (tableau de données -> couche géographique)
musee_geo <- st_as_sf(musee_geo, coords = c("long", "lat"), crs = 4326)
# Chunk 29
# Package pour la cartographie interactive
library(mapview)
# Affichage de la couche géographique des musées
mapview(musee_geo)
# Chunk 30
# Détection/Suppression doublon(s)
data <- data[!duplicated(data$dated), ]
# Une ligne a été supprimée
nrow(data)
# Chunk 31
data$coord
# Chunk 32
# Suppression des chaînes de caractères "Point(" et ")"
data$coord <- gsub(pattern = 'Point\\(', replacement = '', data$coord)
data$coord <- gsub(pattern = '\\)', replacement = '', data$coord)
# Séparation longitude et latitude en deux colonnes
out <- strsplit(data$coord, split = ' ')
data_geo <- cbind(data[-5], do.call(rbind, out))
# Renommage de colonnes créées
colnames(data_geo)[5:6] <- c('long', 'lat')
# Chunk 33
# Affichage des deux nouvelles colonnes créées.
datatable(data_geo[2:6])
# Chunk 34
# Géoréférencement des musées
data_geo <- st_as_sf(data_geo, coords = c("long", "lat"), crs = 4326)
# Affichage de la couche géographique des musées
mapview(data_geo)
# Chunk 35
# Package pour la cartographie thématique
library(tmap)
# Mode cartographie interactive
tmap_mode(mode = "view")
# Carte thématique
tm_basemap() +
tm_shape(musee_geo) +
tm_symbols(size="oeuvreLabel",
scale = 4,
col="red3",
border.col="white",
alpha =0.5,
border.lwd=0.1,
border.alpha=0.5,
title.size = "Nb d'oeuvres",
id = "museeLabel",
popup.vars=c("Nombre d'œuvres"="oeuvreLabel"))
# Chunk 37
# Pour le charger de nouveau :
world <- st_read("data/world/ne_110m_admin_0_countries.shp", quiet = TRUE )
# Chunk 38
# Affichage du fond de carte récupéré
plot(st_geometry(world))
# Chunk 39
# Reprojection du fond de carte pays (polygones)
world <- st_transform(x = world, crs = "+proj=moll", use_gdal = FALSE)
# Reprojection de la couche géographique (points)
musee_geo <- st_transform(musee_geo, crs = st_crs(world))
# Chunk 40
# Jointure spatiale
# Récupération du code ISO3 des pays (uniquement)
musee_geo <-st_intersection(musee_geo, world[,"ADM0_A3"])
# Chunk 41
datatable(st_drop_geometry(musee_geo[2:4]))
# Chunk 42
# Regroupement - Calcul du nombre œuvres par pays
nb_oeuvre_musee_geo <- aggregate(oeuvreLabel ~ ADM0_A3, data = musee_geo, FUN = sum)
# Chunk 43
# Package de représentation graphique
library(ggplot2)
# Graphique en barre - Nombre d'œuvres par pays
ggplot(data = nb_oeuvre_musee_geo, aes(x = ADM0_A3, y = oeuvreLabel, fill=ADM0_A3) )  +
geom_bar(stat="identity") +
ggtitle("Nombre d'œuvres de Johannes Vermeer par pays") +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
# Chunk 44
# Popup content
content <- paste0( "<img  src='http://commons.wikimedia.org/wiki/Special:FilePath/Vermeer%20-%20A%20young%20Woman%20seated%20at%20the%20Virginals.jpg'  width='180'><br /><b>",  data_geo$nomLabel,
"</b><br />Du ",data_geo$dated,
"<br />Au ", data_geo$datef)
# Carte
mapview(data_geo,
zcol = "nomLabel",
col.regions = "red",
legend = FALSE,
popup = content,
map.types = "Esri.WorldImagery")
# Chunk 45
# Reprojection de la couche géographique (points)
data_geo <- st_transform(data_geo, crs = st_crs(world))
# Chunk 46
# Jointure spatial
data_geo <-st_intersection(data_geo, world[,"ADM0_A3"])
# Chunk 47
# Affichage du résultat
datatable(st_drop_geometry(data_geo[2:5]))
# Chunk 48
# Package de représentation graphique
library(ggplot2)
# Graphique en barre - Nombre d'exposition de l'œuvre par pays
ggplot(data = data_geo, aes(factor(ADM0_A3), fill=factor(ADM0_A3)) ) +
geom_bar() +
ggtitle("Nombre d'exposition de l'œuvre par pays") +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
# Chunk 49
# Format date
data_geo$datef <- as.Date(data_geo$datef)
data_geo$dated <- as.Date(data_geo$dated)
# Chunk 50
data_geo <- data_geo[order(data_geo$dated, decreasing = FALSE),]
# Chunk 51
data_geo$ID <- 1:nrow(data_geo)
# Chunk 52
# liste unique musée
musee <- unique(data_geo$nomLabel)
data_geo$nomLabel <- ordered(as.factor(data_geo$nomLabel), levels = musee)
# liste unique pays
countries <- unique(data_geo$ADM0_A3)
data_geo$ADM0_A3 <- ordered(as.factor(data_geo$ADM0_A3), levels = countries)
# Chunk 53
# Label unique pour les pays
museum_label <- data_geo[!duplicated(data_geo$nomLabel),]
data_geo <- merge(data_geo, st_drop_geometry(museum_label[,c("ID","nomLabel")]), by="ID", all.x=TRUE)
# Chunk 54
datatable(data_geo)
# Chunk 55
ggplot(data_geo, aes(x = dated, y = nomLabel.x, color = ADM0_A3))
# Chunk 56
# Construction de 'segments' entre la date d'arrivée (dated) et de départ (datef)
ggplot(data_geo, aes(x = dated, y = nomLabel.x, color = ADM0_A3)) +
geom_segment(aes(xend = datef, yend = nomLabel.x, color =  ADM0_A3), size = 6)
# Chunk 57
Mon_graph <- ggplot(data_geo, aes(x = dated, y = nomLabel.x, color = ADM0_A3)) +
geom_segment(aes(xend = datef, yend = nomLabel.x, color =  ADM0_A3), size = 6)
# Chunk 58
Mon_graph <-  Mon_graph +
# Ajout d'une étiquette pour chaque segment représenté
geom_text(aes(label = nomLabel.y, hjust =1.05), size = 3.5, show.legend = FALSE) +
# Modification de la palette de couleur (pays)
scale_color_manual(values = c("#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d","#1b9e77", "#d95f02"))
# Affichage du résultat
Mon_graph
# Chunk 59
Mon_graph <- Mon_graph +
# Modification de l'axe des abcisse
scale_x_date(date_labels = "%Y", date_breaks = "2 year", minor_breaks = "1 year",
limits = c(as.Date("1994-01-01"),as.Date("2019-01-13")) ) +
# Ajout d'un titre et d'un sous-titre
labs(title = "Le voyage d'un tableau de Johannes Vermeer",
subtitle = "Aux pays des musées, de 2001 à 2019") +
# Paramétrage du 'thème'
theme(panel.grid.major.y = element_blank(),
panel.grid.major.x = element_line(size = 0.2, colour = "#707073"),
panel.grid.minor.x = element_line(size = 0.2, linetype = 3, colour = "#707073"),
panel.border = element_blank(),
panel.background = element_blank(),
axis.text.y = element_blank(),
axis.text.x = element_text(size=8.5, colour = "#FFFFFF"),
axis.title = element_blank(),
rect = element_rect(fill = "#2a2a2b"),
legend.position =  c(.93, .27),
legend.key = element_rect(fill  = "#2a2a2b"),
legend.key.height = unit(0.1, 'cm'),
legend.background = element_rect(fill  = "#2a2a2b"),
legend.margin = margin(0.5,1,1,1),
legend.title = element_blank(),
legend.text = element_text(colour = "#FFFFFF",  size = 7),
title = element_text(colour = "#FFFFFF", hjust = 1, vjust = 0),
plot.margin=unit(c(1,1,0.5,1),"cm"))
Mon_graph
# Chunk 60
# Gestion des images
library(jpeg)
path <- "data/images/Q4660880.jpg"
# Importer une image dans R
img <- readJPEG(path, native = TRUE)
# Chunk 61
# Pour combiner graphiques et images
library(patchwork)
Mon_graph +
# Titre image
annotate("text", x = as.Date("1997-03-01"), y = 7, colour = "#FFFFFF", size = 3.4,
label = "Young Woman Seated\nat a Virginal  (1670)") +
# Insertion de l'image du tableau dans la représentation graphique
inset_element(p = img, clip = TRUE, left = 0.04, right = 0.27, top = 0.95, bottom = 0.46)
# Chunk 62: exo1
## Exemple Picasso
# Indiquez l'identifiant de l'artiste choisi-e entre ""
ID <- "Q5593"
# Indiquez le nom de l'artiste choisi-e entre ""
NOM <- "Pablo Picasso"
## Exemple Dali
# ID <- "Q5577"
# NOM <- "Salvador Dalí"
## Exemple Nandalal Bose (IN)
# ID <- "Q3348980"
# NOM <- "Nandalal Bose"
## Exemple Kalervo Palsa (FI)
# ID <- "Q320590"
# NOM <- "Kalervo Palsa"
## Exemple Edvard Munch
# ID <- "Q41406"
# NOM <- "Edvard Munch"
## Exemple Katsushika Hokusai
# ID <- "Q5586"
# NOM <- "Katsushika Hokusai"
# Chunk 63: exo
# Jouer l'exemple ?
# EXO <- FALSE
EXO <- TRUE
# Chunk 64
knitr::include_graphics('figures/knit.png')
# Chunk 65: exo2
# query en fonction de l'ID
query <-  paste0("SELECT ?oeuvreLabel ?museeLabel ?coord WHERE { SERVICE wikibase:label { bd:serviceParam wikibase:language '[AUTO_LANGUAGE],en'. } ?oeuvre wdt:P170 wd:", ID, " . OPTIONAL {?oeuvre p:P276 ?loc . ?loc ps:P276 ?musee . ?musee wdt:P625 ?coord} . }")
# Affichage de la requête
print(query)
# Chunk 66: exo3
# Requête SPARQL wikidata
my_data <- query_wikidata(query)
# Affichage du résultat
datatable(my_data)
# Chunk 67: exo4
# Détection/Suppression doublon(s)
my_data <- my_data[!duplicated(my_data$oeuvreLabel), ]
# Chunk 68: exo5
# Suppression NA
my_data <- my_data[!is.na(my_data$coord), ]
# Chunk 69: exo6
# Affichage de la table
datatable(my_data)
# Chunk 70: exo7
# Regroupement
nb_oeuvre <- aggregate(oeuvreLabel ~ coord + museeLabel, data = my_data, FUN = length)
# Affichage du résultat
datatable(nb_oeuvre)
# Chunk 71: exo8
# Suppression des chaînes de caractères "Point(" et ")"
nb_oeuvre$coord <- gsub(pattern = 'Point\\(', replacement = '', nb_oeuvre$coord)
nb_oeuvre$coord <- gsub(pattern = '\\)', replacement = '', nb_oeuvre$coord)
# Séparation longitude et latitude en deux colonnes
out <- strsplit(nb_oeuvre$coord, split = ' ')
my_museum <- cbind(nb_oeuvre, do.call(rbind, out))
# Renommage de colonnes créées
colnames(my_museum)[4:5] <- c('long', 'lat')
# Affichage de la table
datatable(my_museum)
# Chunk 72: exo9
# Géoréférencement
my_museum <- st_as_sf(my_museum, coords = c("long", "lat"), crs = 4326)
# Chunk 73: exo10
# Reprojection de la couche géographique des musées (points)
my_museum <- st_transform(my_museum, crs = st_crs(world))
# Chunk 74: exo11
# Jointure spatiale
my_museum <-st_intersection(my_museum, world[,"ADM0_A3"])
# Chunk 75: exo12
# Regroupement pars pays
by_country <- aggregate(oeuvreLabel ~ ADM0_A3 , data = my_museum, FUN = sum)
# Affichage de la table
datatable(by_country)
ggplot(data = by_country , aes(factor(ADM0_A3), oeuvreLabel) ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(factor(ADM0_A3), oeuvreLabel) ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel, fill = factor(ADM0_A3)) ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel, fill = ADM0_A3) ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel) ) +
geom_bar(stat = "identity", fill = ADM0_A3) +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel), fill = ADM0_A3 ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel), fill = ADM0_A3 ) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel), fill = "ADM0_A3") +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel), fill = factor(ADM0_A3)) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country , aes(x = factor(ADM0_A3), y =  oeuvreLabel), fill = factor(ADM0_A3)) +
geom_bar(stat = "identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel, fill=ADM0_A3) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel, fill=ADM0_A3) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
scale_fill_brewer(palette="Set1") +
theme(legend.position = "none")
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none")
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none")
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none", axis.text=element_text(size=12))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none", axis.text=element_text(size=7))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none",
axis.text.x = =element_text(size=8))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none",
axis.text.x = element_text(size=8))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none",
axis.text.x = element_text(size=8),
axis.text.y = element_text(size=12))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none",
axis.text.x = element_text(size=8),
axis.text.y = element_text(size=10))
ggplot(data = by_country, aes(x = ADM0_A3, y = oeuvreLabel) )  +
geom_bar(stat="identity") +
ggtitle(paste0("Nombre d'oeuvres de ", NOM ,", par pays")) +
xlab("") +
ylab("") +
theme(legend.position = "none",
axis.text.x = element_text(size=8),
axis.text.y = element_text(size=11))
## Copie le code contenu dans les chunks dans un fichier script simple
## mon_notebook_final.R
knitr::purl("mon_notebook_final.Rmd", documentation = 0)
## Copie le code contenu dans les chunks dans un fichier script simple
## mon_notebook_final.R
knitr::purl("mon_notebook_final.Rmd", documentation = 0)
