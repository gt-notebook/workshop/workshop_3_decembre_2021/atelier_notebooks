# Atelier notebooks

L'atelier « Les Notebooks, enjeux et problématiques en lien avec le cycle des données de la recherche » a été présenté lors de la [Journée d'étude normande sur les données de la recherche](https://jedata-normande.sciencesconf.org/), le vendredi 3 décembre 2021 à l'[IMEC](https://www.imec-archives.com/).

Lors de la partie pratique de cet atelier, vous allez expérimenter deux types de notebooks. Un notebook Jupyter Python dans l'interface Jupyter Lab et un notebook Rmarkdown dans l'interface RStudio. 

Nous avons créé un site en ligne qui contient l'environnement Python et R adéquat et qui vous permettra d'expérimenter ces notebooks. Vous n'avez rien à installer sur votre machine. Cliquez simplement sur les boutons proposés et patientez quelques secondes.

Lien court vers cette page : <https://page.hn/gurt5d>

## Notebook Jupyter Python

Cliquez sur le bouton ci-contre pour lancer Jupyter Lab : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fgt-notebook%2Fworkshop%2Fworkshop_3_decembre_2021%2Fatelier_notebooks.git/HEAD).

Ouvrez ensuite le notebook `notebook_covid-19.ipynb`.

## Notebook Rmarkdown

Cliquez sur le bouton ci-contre pour lancer RStudio : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fgt-notebook%2Fworkshop%2Fworkshop_3_decembre_2021%2Fatelier_notebooks.git/HEAD?urlpath=rstudio)

Ouvrez ensuite le notebook `mon_notebook_final.Rmd` dans le répertoire `Rmarkdown`.

## Après l'atelier

Si vous souhaitez installer les différents logiciels et obtenir les notebooks pour reproduire cet atelier, nous vous invitons à consulter cette [documentation](installation.md).
