La procédure ci-dessous est basée sur un système d'exploitation de type Linux Ubuntu. Si vous êtes sous Mac OSX ou Windows, vous devriez pouvoir reproduire quelque chose d'équivalent moyennant quelques adaptations.

## Étape 1 : Installer miniconda

Téléchargez [miniconda](https://docs.conda.io/en/latest/miniconda.html) pour Python 3.9 depuis le terminal :
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.10.3-Linux-x86_64.sh
```

Installez miniconda en lancant le script `.sh` depuis le répertoire de téléchargement : 
```bash
bash ./Miniconda3-py39_4.10.3-Linux-x86_64.sh
```

Répondre `yes` à : `Do you wish the installer to initialize Miniconda3 by running conda init? [yes|no]`

Fermez votre terminal puis ouvrez-en un nouveau.

Vérifiez que conda est bien installé avec la commande :
```bash
conda --version
```

## Étape 2 : Installer mamba 

[Mamba](https://github.com/mamba-org/mamba) est une extension de conda qui permet d'installer des environnements et des logiciels beaucoup plus rapidement.

```bash
conda install mamba -n base -c conda-forge
```

## Étape 3 : Cloner le dépôt contenant les fichiers de l'atelier

La logiciel git doit être installé au préalable sur votre machine.

Placez-vous préalablement dans votre répertoire de travail.

Puis clonez le dépôt de l'atelier depuis GitLab :

```bash
git clone https://gitlab.huma-num.fr/gt-notebook/workshop/workshop_3_decembre_2021/atelier_notebooks.git
cd atelier_notebooks
```

## Étape 4 : Créer l'environnement conda

```bash
mamba env create --file binder/environment.yml
```

## Étape 5 : Activer l'environnement conda

```bash
conda activate atelier-notebooks
```

## Étape 6 : Exploration des notebooks

### Notebook Python

Pour lancer Jupyter Lab, exécutez la commande suivante dans un terminal :
```bash
jupyter lab
```
Dans l'interface Jupyter Lab, ouvrez ensuite le notebook `notebook_covid-19.ipynb`.

Les modifications abordées au notebooks sont persistantes.

Pour quitter Jupyter Lab, tapez deux fois de suite <kbd>Ctrl</kbd> + <kbd>C</kbd> dans le terminal depuis lequel vous avez lancé Jupyter Lab.

### Notebook Rmarkdown

La logiciel RStudio doit être installé au préalable sur votre machine.

Lancez RStudio puis ouvrez le notebook `mon_notebook_final.R` dans le répertoire `Rmarkdown`.

Les modifications abordées au notebooks sont persistantes.


## Étape 7 : Sortir de l'environnement conda

Exécutez la commande suivante dans un terminal pour quitter l'environnement conda créé pour l'atelier :

```bash
conda deactivate
```

Documentation pour gérer les environnements conda : https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file

